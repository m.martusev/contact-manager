﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager
{
    class Program
    {
        private const String contactsFile = "contacts.txt";
        private static ContactManager contactManager;

        static void Main(string[] args)
        {
            try
            {
                contactManager = new ContactManager(contactsFile);
            }
            catch
            {
                Console.Out.WriteLine("Failed to open file - press any key to exit program");
                Console.ReadKey();
                return;
            }

            String action = "";
            Console.Out.WriteLine("*Type the number of the desired action and hit enter*");

            while (!action.Equals("5"))
            {
                Console.Out.WriteLine("[1] Add contact");
                Console.Out.WriteLine("[2] Update contact information");
                Console.Out.WriteLine("[3] Delete contact");
                Console.Out.WriteLine("[4] View contacts");
                Console.Out.WriteLine("[5] Close program");

                action = Console.ReadLine();

                switch (action)
                {
                    case "1":
                        AddContact();
                        break;
                    case "2":
                        UpdateContact();
                        break;
                    case "3":
                        DeleteContact();
                        break;
                    case "4":
                        ViewContacts();
                        break;
                    default:
                        Console.Out.WriteLine("Invalid action.");
                        break;

                }

            }

        }


        private static void AddContact()
        {
            try
            {
                Console.Out.WriteLine("Enter first name");
                String firstName = Console.ReadLine();

                Console.Out.WriteLine("Enter last name");
                String lastName = Console.ReadLine();

                Console.Out.WriteLine("Enter phone number");
                String phoneNumber = Console.ReadLine();

                Console.Out.WriteLine("Enter address");
                String address = Console.ReadLine();

                contactManager.AddContact(firstName, lastName, phoneNumber, address);
                contactManager.Save();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Failed to add contact: " + e.Message);

            }

        }

        private static void UpdateContact()
        {
  
            try
            {
                Console.Out.WriteLine("Which contact do you want to update? (choose a number)");
                ViewContacts();
                int id = Int32.Parse(Console.ReadLine());

                Console.Out.WriteLine("Enter first name");
                String firstName = Console.ReadLine();

                Console.Out.WriteLine("Enter last name");
                String lastName = Console.ReadLine();

                Console.Out.WriteLine("Enter phone number");
                String phoneNumber = Console.ReadLine();

                Console.Out.WriteLine("Enter address");
                String address = Console.ReadLine();

                contactManager.UpdateContact(id, firstName, lastName, phoneNumber, address);
                contactManager.Save();

            }
            catch(Exception e)
            {
                 Console.Out.WriteLine("Failed to update contact: " + e.Message);

            }

        }

        private static void DeleteContact()
        {
            try
            {

                Console.Out.WriteLine("Which contact do you want to delete? (choose a number)");
                ViewContacts();
                int id = Int32.Parse(Console.ReadLine());
                contactManager.DeleteContact(id);
                contactManager.Save();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Failed to delete contact: " + e.Message);

            }

        }

        private static void ViewContacts()
        {
            List<Contact> contacts = contactManager.Contacts;
            Console.Out.WriteLine("----------Contacts-----------");
            Console.Out.WriteLine("Name|Last Name|Phone Number|Address");

            for (int i = 0; i < contacts.Count; i++)
                Console.Out.WriteLine($"[{i}] {contacts[i].ToString()}");
            Console.Out.WriteLine("-----------------------------");
        }

    }
}
