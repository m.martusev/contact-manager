﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager
{
    class Contact
    {
        private String firstName;
        private String lastName;
        private String phoneNumber;
        private String address;

        public String FirstName
        {
            get { return firstName; }
        }

        public String LastName
        {
            get { return lastName; }
        }

        public String PhoneNumber
        {
            get { return phoneNumber; }
        }

        public String Address
        {
            get { return address; }
        }


        public Contact(String firstName, String lastName, String phoneNumber, String address)
        {

            if (firstName == "" || lastName == "" || phoneNumber == "")
                throw new Exception("First name, last name and phone number are required.");

            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.address = address;
        }

        public override String ToString()
        {
            return $"{firstName}|{lastName}|{phoneNumber}|{address} ";
        }

    }
}
