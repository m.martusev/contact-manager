﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager
{
    class ContactManager
    {

        private String contactsFile;
        private List<Contact> contacts;

        public List<Contact> Contacts
        {
            get
            {
                return contacts;
            }
        }

        public ContactManager(String contactsFile)
        {
           
                this.contactsFile = contactsFile;
                contacts = new List<Contact>();
                foreach (String contact in System.IO.File.ReadAllLines(this.contactsFile))
                {
                    String[] contactData = contact.Split('|');
                    contacts.Add(new Contact(contactData[0], contactData[1], contactData[2], contactData[3]));

                }
                           
        }

        public void AddContact(String firstName, String lastName, String phoneNumber, String address)
        {
            Contact contact = new Contact(firstName, lastName, phoneNumber, address);
 
                ValidateAdd(contact);
                contacts.Add(contact);
            
        }

        public void UpdateContact(int id, String firstName, String lastName, String phoneNumber, String address)
        {
            Contact contact = new Contact(firstName, lastName, phoneNumber, address);

                ValidateUpdate(contact, id);
                contacts[id] = contact;

        }


        public void DeleteContact(int id)
        {

                contacts.RemoveAt(id);

        }

        private void ValidateAdd(Contact newContact)
        {
            var contactsWithSameNumber = contacts.Where(c => c.PhoneNumber == newContact.PhoneNumber);
            if (contactsWithSameNumber.Count() != 0)
                throw new Exception("A contact with the same phone number already exists");

        }

        private void ValidateUpdate(Contact newContact, int id) {
            List<Contact> oldContact = new List<Contact> { contacts.ElementAt(id) };
            var contactsWithSameNumber = contacts.Where(c => c.PhoneNumber == newContact.PhoneNumber).Except(oldContact);//ignore the number that's being updated

            if (contactsWithSameNumber.Count() != 0)
                throw new Exception("A contact with the same phone number already exists");

        }
        

        public void Save()
        {
            
            System.IO.StreamWriter file = new System.IO.StreamWriter(contactsFile);
            
            foreach (Contact contact in contacts) {
                file.WriteLine(contact.ToString());
            }
            file.Close();
        }
    }
}
